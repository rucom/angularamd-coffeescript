define [
  "app"
  "angularAMD"
  'angular'
  'angular-ui-router'
], (app, angularAMD) ->
  describe "app.js", ->
    it "angularAMD should be defined", ->
      expect(angularAMD).toBeDefined()
      return

    it "angular app should be defined", ->
      expect(app.name).toEqual "app"
      return

    return

  return
