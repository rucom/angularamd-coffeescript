define [
	'angularAMD'
	'angular'
	'angular-ui-router'
], (angularAMD) ->
	'use strict'
	app = angular.module 'app', ['ui.router']

	app.config [
		'$stateProvider'
		'$urlRouterProvider'
		($stateProvider, $urlRouterProvider) ->
			$stateProvider
				.state 'home', angularAMD.route
					url: '/home'
					templateUrl: '../views/home.html'
					controller: 'homeController'
					controllerUrl: 'main/home_ctrl'

			$urlRouterProvider.otherwise '/home'
			return
	]

	angularAMD.bootstrap app
	app